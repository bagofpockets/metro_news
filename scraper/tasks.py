import traceback
from datetime import datetime, timedelta
import requests
from celery import states
from src.worker import celery, pg_session
from src.context import request_id as ctx_request_id
from src.utils import get_news
from src.db.schemas import News


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(60.0 * 10, get_page.s(), expires=10)


class BaseTask(celery.Task):
    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        pg_session.remove()


@celery.task(name='news.get_page', bind=True, base=BaseTask)
def get_page(self, request_id: str = 'scheduled'):
    ctx_request_id.set(request_id)
    response = {}
    try:
        raw_news = requests.get('http://mosday.ru/news/tags.php?metro')
        if not raw_news.status_code == 200:
            response_fail = {
                'fail_type': 'get request status is not 200',
                'fail_message': f'{raw_news.status_code} | {raw_news.headers} | {raw_news.text}'
            }
            self.update_state(
                state=states.FAILURE,
                response=response_fail)
            response.update(response_fail)
        else:
            news_objs = get_news(raw_news.text)
            pg_session.add_all(news_objs)
            pg_session.commit()
        return response
    except Exception as ex:
        response_fail = {
            'exc_type': type(ex).__name__,
            'exc_message': traceback.format_exc().split('\n')
        }
        self.update_state(
            state=states.FAILURE,
            response=response_fail)
        response.update(response_fail)
        return response


@celery.task(name='news.get_last', bind=True, base=BaseTask)
def get_page(self, request_id: str, days: int):
    ctx_request_id.set(request_id)
    response = {}
    try:
        news = pg_session.query(News).filter(
            News.timestamp >= (datetime.today() - timedelta(days=days)).date().strftime('%Y-%m-%d'))
        response["result"] = [n.as_dict() for n in news]
        return response
    except Exception as ex:
        response_fail = {
            'exc_type': type(ex).__name__,
            'exc_message': traceback.format_exc().split('\n')
        }
        self.update_state(
            state=states.FAILURE,
            response=response_fail)
        response.update(response_fail)
        return response
