from bs4 import BeautifulSoup, Tag
from datetime import datetime
from src.db.schemas import News


def table_filter(tag: Tag) -> bool:
    if tag.name == 'table' and all(tag.has_attr(key) and tag[key] == value for key, value in
                                   [("width", "95%"), ("cellpadding", "0"), ("cellspacing", "10"),
                                    ("border", "0"), ("style", "font-family:Arial;font-size:15px")]):
        return True
    return False


def get_news(html_text: str) -> list[News]:
    result = []
    soup = BeautifulSoup(html_text, 'html.parser')
    tables = soup.find_all(table_filter)
    for table in tables:
        for child in table.children:
            img_url = child.find('img')
            if not img_url == -1:
                tds = child.find_all('td')
                bs = tds[1].find_all('b')
                if bs:
                    date = bs[0].text
                    title = bs[1].text
                    content_with_time = child.find('font').contents
                    if isinstance(content_with_time[1], str):
                        time = content_with_time[1].strip()
                    else:
                        time = '00:00'
                    news = News(
                        title=title,
                        image_url=img_url if img_url is None else img_url.get("src"),
                        timestamp=datetime.strptime(f'{date} {time}', '%d.%m.%Y %H:%M')
                    )
                    result.append(news)
    return result
