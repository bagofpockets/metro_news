from sqlalchemy import Column, DateTime, Integer, String, UniqueConstraint
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class News(Base):
    __tablename__ = "news"
    __table_args__ = (
        UniqueConstraint('title', 'image_url', 'timestamp'),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    image_url = Column(String)
    timestamp = Column(DateTime)

    def as_dict(self):
        return {
            "title": self.title,
            "image_url": f'https://mosday.ru/news/{self.image_url}' if self.image_url else '',
            "timestamp": self.timestamp,
        }
