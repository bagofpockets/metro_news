import os
from celery import Celery
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker, scoped_session
from src.db.schemas import Base

RABBIT_DSN: str = os.getenv('RABBITMQ_URI')
PG_DSN: str = f"{os.getenv('PG_URI')}"
PG_BACKEND_DSN: str = f"db+{os.getenv('PG_BACKEND_URI')}"

pg_engine = create_engine(PG_DSN, echo=True, pool_recycle=3600, pool_size=10)
Base.metadata.create_all(pg_engine)
pg_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=pg_engine))

celery_pg_engine = create_engine(os.getenv('PG_BACKEND_URI'), echo=True)
if not database_exists(celery_pg_engine.url):
    create_database(celery_pg_engine.url)
celery = Celery("news_tasks", backend=PG_BACKEND_DSN, broker=RABBIT_DSN)
