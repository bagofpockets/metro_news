import logging
from src.context import request_id


class ContextFilter(logging.Filter):
    def filter(self, record):
        record.request_id = request_id.get()
        record.service = 'scraper'
        return True


logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s:     %(service)s | UTC %(asctime)s | %(request_id)s | %(message)s')
logger = logging.getLogger(__name__)
logger.addFilter(ContextFilter())
