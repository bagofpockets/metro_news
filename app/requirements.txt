# API server
fastapi==0.103.1
uvicorn==0.23.2
python-dotenv==1.0.0
pydantic_settings==2.0.3

# Tasks
celery==5.3.4
psycopg2-binary==2.9.7
sqlalchemy==2.0.20
