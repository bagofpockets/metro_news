from fastapi import APIRouter, status, HTTPException
from celery.result import AsyncResult
from core import celery
from context import request_id

router = APIRouter()


@router.get(path='/news', status_code=status.HTTP_200_OK)
async def get_news(day: int) -> list:
    task = celery.send_task("news.get_last", args=[request_id.get(), day])
    result = AsyncResult(task.id, app=celery).get()

    if not result:
        raise HTTPException(status_code=500, detail=f"Something went wrong with task.")
    return result["result"]
