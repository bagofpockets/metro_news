from fastapi import APIRouter
from core import settings
from api.routes.news import router as news_router

router = APIRouter()

router.include_router(news_router, prefix=settings.NEWS_PREFIX, tags=["News"])
