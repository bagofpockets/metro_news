import os
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    LOCAL_DEBUG: bool = os.getenv('LOCAL_DEBUG', 'False').lower() == 'true'
    PROJECT_NAME: str = "metro-news-server"
    VERSION: str = "0.0.1"
    API_PREFIX: str = "/api"
    NEWS_PREFIX: str = "/news"
    RABBIT_DSN: str = os.getenv('RABBITMQ_URI')
    PG_BACKEND_DSN: str = f"db+{os.getenv('PG_BACKEND_URI')}"


settings = Settings()
