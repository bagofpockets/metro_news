from celery import Celery
from core.config import settings

celery = Celery("celery", backend=settings.PG_BACKEND_DSN, broker=settings.RABBIT_DSN)
