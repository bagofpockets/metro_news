from typing import Callable
from fastapi.applications import FastAPI
from api.logger import logger
from context import request_id, client


def create_start_app_handler(app: FastAPI) -> Callable:
    async def start_app() -> None:
        request_id.set('system')
        client.set('localhost')
        logger.info('Started.')

    return start_app


def create_stop_app_handler(app: FastAPI) -> Callable:
    async def stop_app() -> None:
        request_id.set('system')
        client.set('localhost')
        logger.info('Stopping...')

    return stop_app
