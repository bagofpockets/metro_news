from contextvars import ContextVar

request_id: ContextVar[str] = ContextVar('request_id')
client: ContextVar[str] = ContextVar('client')
